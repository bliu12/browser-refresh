import { AureliaCookie } from "aurelia-cookie";
import AdminLTE from "admin-lte";
import {Router, RouterConfiguration} from "aurelia-router";
import {inject, bindable} from "aurelia-framework";
import {EventAggregator} from "aurelia-event-aggregator";
import {relativeToFile} from "aurelia-path";

@inject(EventAggregator)
export class Group {
	activatedTab = [];
	panelName = "Group Manager";

	mainPageCSS = "";

	router: Router;

	// event handler for creating new accounts
	eventAggregator: EventAggregator;

	configureRouter(config: RouterConfiguration, router: Router) {
		config.map([
		{
			"route": "",
			"name": "listGroup",
			"moduleId": "list-group"
		},
		{
			"route": "add",
			"name": "addGroup",
			"moduleId": "add-group"
		},
		{
			"route": ["group/:id"],
			"name": "editGroup",
			"moduleId": "edit-group"
		},
		{
			"route": ["share/:id/:base?"],
			"name": "shareGroup",
			"moduleId": "share",
			settings: {base: 'g'},
		},
		{
			"route": ["members/:id"],
			"name": "membersGroup",
			"moduleId": "members-group"
		},
		{
			"route": ["appSetting/:id"],
			"name": "groupAppSetting",
			"moduleId": "group-app-setting"
		},
		{
			"route": ["addUser/:id"],
			"name": "addUser",
			"moduleId": "add-users"
		},
	]);

		this.router = router;
	}

	attached() {
		console.log(this.router.parent.routes);
		let scope = this;

		this.eventAggregator.subscribe('groupPage', data => {

			console.log("Now navigating to inner route:", data.route);
			scope.router.navigate(data.route + '/g');
		});

		this.eventAggregator.subscribe('fullScreen', data => {
			if (data.isFullScreen) {
				scope.mainPageCSS = 'margin-left: 0px;';
			} else {
				scope.mainPageCSS = 'margin-left: 230px;';
			}
			this.eventAggregator.publish("showSidePanel", {
				show: !data.isFullScreen,
			});
		});

		this.eventAggregator.subscribe('addGroup', data => {
			scope.router.navigate('add');
		});

		this.eventAggregator.subscribe('addMember', data => {
			scope.router.navigate('addUser/' + data.id);
		});
	}

	constructor(eventAggregator) {
		this.eventAggregator = eventAggregator;
	}

	activate(params) {
		this.activatedTab = [];
		this.panelName = "";

		this.mainPageCSS = 'margin-left: 0px;';
		// console.log("history:", this.router.history.location.hash);

		// let path = this.router.history.location.hash;

		// let pathArray = path.split("/");

		// console.log("path array:", pathArray);

		// let mainPage = true;
		// for (let pathPart in pathArray) {
		// 	if (pathPart === 'share' || pathPart === 'group') {
		// 		mainPage = false;
		// 		break;
		// 	}
		// }

		// if (mainPage) {
		// 	this.eventAggregator.publish('fullScreen', { isFullScreen: false});
		// } else {
		// 	this.eventAggregator.publish('fullScreen', { isFullScreen: true });
		// }

		// this.eventAggregator.publish('fullScreen', { isFullScreen: true });
		this.eventAggregator.publish("setTopTab", { tab: 0 });
	}

	clickedTab(tab) {
		// for (let a = 0; a < this.sideTabs.length; a += 1) {			
		// 	if (this.sideTabs[a] === tab) {
		// 		this.sideTabs[a].active = true;
		// 	} else {
		// 		this.sideTabs[a].active = false;
		// 	}
		// }

		// Navigate to the correct page
		// this.router.navigate(tab.route);
	}
}
