import {Redirect} from "aurelia-router";
import {inject, bindable, observable} from "aurelia-framework";
import {EventAggregator} from "aurelia-event-aggregator";
import {AureliaCookie} from "aurelia-cookie";
import {RedirectToRoute} from "aurelia-router";
import environment from './environment';

@inject(EventAggregator)
export class App {
	
	@bindable
	loggedIn;	
	
	eventAggregator: EventAggregator;
	router: Redirect;

	constructor(eventAggregator) {
		this.eventAggregator = eventAggregator;
	}
	
	configureRouter(config, router) {
		// config.options.pushState = false;
		
		config.map([ {
				"route": "",
				"name": "hub",
				"moduleId": "hub",
				"nav": true,
				"title": "Hub",
				// this is going to be available within AuthorizeStep
				"settings": {
					"auth": false,
					"publicOnly": false
				}
			}
		]);
		
		// The 404 page
		config.mapUnknownRoutes("404.html");
		
		config.addPipelineStep('authorize', AuthorizeStep);
		
		this.router = router;
		this.loggedIn = false;
	}
	
	attached() {
		this.eventAggregator.subscribe('nav::toggleLogin', (data) => {
			AuthorizeStep.auth.isAuthenticated = data.loggedIn;
		});
	}

	activate(params, queryString) {
		if ((AureliaCookie.get("ParseSession") != null && AureliaCookie.get("ParseSession") != "") &&
			(AureliaCookie.get("ParseId") != null && AureliaCookie.get("ParseId") != "")) {
			AuthorizeStep.auth.isAuthenticated = true;
		}
	}
}

class AuthorizeStep {
	static auth = {
		isAuthenticated: false
	}

	run(navigationInstruction, next) {
		// let isLoggedIn = AuthorizeStep.auth.isAuthenticated;
		
		// currently active route config
		// let currentRoute = navigationInstruction.config;
		
		// settings object will be preserved during navigation
		// let loginRequired = currentRoute.settings && currentRoute.settings.auth === true;
		
		// if (isLoggedIn === false && loginRequired === true) {
		// 		return next.cancel(new RedirectToRoute('login'));
		// }
		
		// let publicOnly = currentRoute.settings && currentRoute.settings.publicOnly === true;
		// if (isLoggedIn === true && publicOnly === true) {
		// 	return next.cancel(new RedirectToRoute('hub'));
		// }
		
		return next();
	}	
}
