import { AureliaCookie } from "aurelia-cookie";
import AdminLTE from "admin-lte";
import {inject, bindable} from "aurelia-framework";
import {EventAggregator} from "aurelia-event-aggregator";

@inject(EventAggregator)
export class Group {
	groupList = [];
	checkAll = false;
	checkAllPage = false;
	paginationPages;
	currentPaginationPage = 0;
	maxPaginationList = 50;
	lastItem = 0;
	paginatedList = [];
	errorMessage = "";
	activatedTab = [];
	accountType = "";

	// event handler for creating new accounts
	eventAggregator: EventAggregator;

	attached() {
		let scope = this;
		this.eventAggregator.subscribe('updateGroups', (data) => {
			scope.activate(null);
		});
	}

	constructor(eventAggregator) {
		this.eventAggregator = eventAggregator;
	}

	async activate(params) {

		let objectId = null;
		let expire = null;

		for (let parameter in params) {
			if (parameter === "o") {
				objectId = params[parameter];
			}
			if (parameter === "t") {
				expire = params[parameter];
			}
		}

		this.groupList = [];
		this.checkAll = false;
		this.checkAllPage = false;
		this.paginationPages;
		this.currentPaginationPage = 0;
		this.maxPaginationList = 50;
		this.lastItem = 0;
		this.paginatedList = [];
		this.errorMessage = "";
		this.activatedTab = ['active', '', ''];

		try {
			// let getGroupList = await Parse.Cloud.run("getGroupList", {
			// 	user: AureliaCookie.get("ParseId"),
			// 	token: AureliaCookie.get("ParseSession"),
			// 	showAll: 'y'
			// });
			this.groupList = [
				{name: "1", id: 1, appIds: '1'},
				{name: "2", id: 2, appIds: '2'},
				{name: "3", id: 3, appIds: '3'},
				{name: "4", id: 4, appIds: '4'},
			];

		
			let a = 0;

			if (this.maxPaginationList % this.groupList.length !== 0) {
				a = 1;
			}

			if (this.groupList.length < this.maxPaginationList + 1) {
				this.lastItem = Math.min(this.groupList.length, this.maxPaginationList);
			} else {
				this.lastItem = this.maxPaginationList;
			}

			this.paginationPages = Math.round(this.groupList.length / this.maxPaginationList + a);
			this.paginatedList = this.groupList.slice(0, this.maxPaginationList);
		} catch(error) {
			console.log("Could not get groups!");
		}

		this.accountType = 'admin';
		this.eventAggregator.publish('fullScreen', { isFullScreen: true });
		this.eventAggregator.publish("setTopTab", { tab: 0 });
	}

	showDetails(group) {
		console.log("Group id:", group.id);
		
		this.eventAggregator.publish("groupPage", {
			route: "group/" + group.id,
		});
		// this.router.navigate("group/" + group.id);
	}
}
