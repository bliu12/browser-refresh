import {AureliaCookie} from "aurelia-cookie";
import {Router, RouterConfiguration} from "aurelia-router";
import {inject, bindable, observable} from "aurelia-framework";
import {EventAggregator} from "aurelia-event-aggregator";
import AdminLTE from "admin-lte";

@inject(Router, EventAggregator)
export class App {
	@bindable
	loggedIn;

	eventAggregator: EventAggregator;
	hubRouter: Router;
	mainRouter: Router;

	actions = [];

	userName = "";
	picture = "";
	email = "";
	hamburgerCSS = "";
	windowWidth = 0;

	sidePanelCSS = "";
	panelName = "";
	panelPicture = "";
	sideTabs = [];

	constructor(router, eventAggregator) {
		this.mainRouter = router;
		this.eventAggregator = eventAggregator;
	}

	configureRouter(config: RouterConfiguration, router: Router) {
		config.map([
			{
				"route": "users",
				"name": "users",
				"moduleId": "hub-users"
			},
			{
				"route": ["groups", ""],
				"name": "Group Manager",
				"moduleId": "hub-group",
			},
			{
				"route": "apps",
				"name": "apps",
				"moduleId": "hub-apps"
			}
		]);

		config.mapUnknownRoutes("404.html");

		this.hubRouter = router;
	}

	attached() {
		let scope = this;

		this.eventAggregator.subscribe('updateProfile', (data) => {
			scope.picture = data.picture;
		});

		this.eventAggregator.subscribe('updateUsername', (data) => {
			scope.userName = data.userName;
		});

		this.eventAggregator.subscribe('showSidePanel', data => {
			// data.show is a boolean.
			console.log("panel before changing side panel info:", scope.sidePanelCSS);

			if (data.show) {
				scope.sidePanelCSS = '';
			} else {
				scope.sidePanelCSS = 'display: none;';
			}
			console.log("panel AFTER changing side panel info:", scope.sidePanelCSS);
			console.log("show side panel:", data.show);
		});

		this.eventAggregator.subscribe('replaceSidePanel', data => {
			scope.sideTabs = [];
			console.log("data.tabs:", data.tabs)
			// data.tabs is an array of all of the tab data.
			for (let tab in data.tabs) {
				let temp = data.tabs[tab];
				scope.sideTabs.push( {
					name: temp.name,
					route: temp.route,
					active: temp.active,
					base: temp.base
				});
			}

			console.log("side tabs:", scope.sideTabs[0].active);
		});

		this.eventAggregator.subscribe('activateGroupTab', data => {
			// data.tab is an integer representing the tab index to activate
			for (let a = 0; a < scope.sideTabs.length; a += 1) {
				scope.sideTabs[a].active = false;
			}
			scope.sideTabs[data.tab].active = true;
		});

		this.eventAggregator.subscribe('replaceSideTitle', data => {
			scope.panelName = data.name;
			scope.panelPicture = data.icon;
		});

		this.eventAggregator.subscribe('setTopTab', data => {
			// Activate the correct tab and deactivate all of the others.
			for (let a = 0; a < scope.actions.length; a += 1) {
				scope.actions[a].active = false;
			}

			// Set the correct tab active. This compensates for different users
			// having a different number of tabs.
			if (data.tab >= scope.actions.length) {
				scope.actions[scope.actions.length - 1].active = true;
			} else {
				scope.actions[data.tab].active = true;
			}
		});
	}

	async activate(params) {

		this.userName = 'admin';
		this.picture = '';
		this.email = '';
		
		this.sideTabs = [];
		this.sidePanelCSS = "";
		this.panelName = "";
		this.sidePanelCSS = 'display: none;';
	}

	async tabClick(tab) {
		// Activate the correct tab and deactivate all of the others.
		for (let a = 0; a < this.actions.length; a += 1) {
			if (a === this.actions.indexOf(tab)) {
				this.actions[a].active = true;
			} else {
				this.actions[a].active = false;
			}
		}

		if (tab.name === 'Apps') {
			this.hubRouter.navigate('apps');
		} else if (tab.name === 'Groups') {
			this.hubRouter.navigate('groups');
		} else if (tab.name === 'Users') {
			this.hubRouter.navigate('users');
		} else {
			this.hubRouter.navigate('');
		}
	}

	async signOut() {
		AureliaCookie.delete("ParseSession");
		AureliaCookie.delete("ParseId");
		this.eventAggregator.publish("nav::toggleLogin", {loggedIn: false});
		this.mainRouter.navigate("/");
	}

	updateWindowSize(detail) {
		// console.log(detail);
		this.windowWidth = detail.width;
	}

	showSidePanel() {
		this.eventAggregator.publish("showSidePanel", {show: false});
	}

	clickedTab(tab) {
		console.log("clicked side pane tab:", tab);
		console.log("Router", this.hubRouter.currentInstruction.config.name);
		if (this.hubRouter.currentInstruction.config.name === "apps") {
			this.eventAggregator.publish('appPage', { route: tab.route });
		} else {
			console.log("going to group")
			this.eventAggregator.publish('groupPage', {route: tab.route});
		}
	}
}
